const express = require('express')
const router = express.Router()
const {lista_tarefas, Tarefa} = require('../../models/tarefa')
const cors = require('cors')

router.get('/', cors(), (req, res, next) => {
    try{
        res.send(lista_tarefas)
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.post('/', [], (req, res, next) => {
    try{
        let body = req.body
        for (const [chave, valor] of Object.entries(body)){
            if (valor =="" || valor == null){
                res.status(400).send({"error" : "favor preencher todos os campos"})
                break
            }
        }

        if(!res.headersSent){
            let task = new Tarefa(body.tarefa, body.descricao, body.importancia, body.limite)
            lista_tarefas.push(task)
            res.send(lista_tarefas)
        }

    }catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

router.get('/:taskId', (req, res, next) => {
    try{
        let task = lista_tarefas.filter(t => t.id == req.params["taskId"])
        if (task.length > 0){
            res.send(task[0])
        }else{
            res.status(404).send({"error" : "task does not exist"})
        }
    }catch(err){
        console.error(err.message)
        res.status(500).send({"error" : "Server Error"})
    }
})

module.exports = router