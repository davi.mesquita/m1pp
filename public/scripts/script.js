const urlServer = 'http://localhost:3000/tarefa'

const tbody = document.querySelector('#lista table tbody')
console.log(tbody)

function createTr(item) {
    const elementTr = document.createElement('tr')
    elementTr.innerHTML = `
        <td>${item.tarefa}</td>
        <td>${item.descricao}</td>
        <td>${item.importancia}</td>
        <td>${item.registro}</td>
        <td>${item.limite}</td>
    `
    return elementTr
}

async function getList() {

    const result = await fetch(urlServer)
    //console.log(result)
    const data = await result.json()
   // console.log(data)

    data.map(item => tbody.appendChild(createTr(item)))
}