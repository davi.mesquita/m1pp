class Tarefa {
    constructor(tarefa, descricao, importancia, limite){
        this.id = lista_tarefas.length + 1
        this.tarefa = tarefa
        this.descricao = descricao
        this.importancia = importancia
        this.registro = hoje()
        this.limite = limitFormat(limite)
    }
}

let lista_tarefas = []
lista_tarefas.push(new Tarefa('Dar bom dia', 'Um bom dia muito bem dado', 'Tranquilo', '2020-12-20'))

function hoje(){ //pega a data do momento que o registro é feito e formata como DD/MM/YYYY
    let today = new Date()
    let day = today.getDate()
    let month = today.getMonth() + 1
    const year = today.getFullYear()
    
    if(day < 10){
        day = '0' + day
    }
    
    if(month < 10){
        month = '0' + month
    }
    
    today = `${day}/${month}/${year}`
    
    return today
}

function limitFormat(limit){ //limite entra como YYYY-MM-DD e sai como DD/MM/YYYY
    let limitEdit = limit.split("-") 
    limit = `${limitEdit[2]}/${limitEdit[1]}/${limitEdit[0]}`
    return limit
}

module.exports = {lista_tarefas, Tarefa}
