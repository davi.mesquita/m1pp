const express = require('express')
var bodyParser = require('body-parser')
const app = express()
const PORT = 3000
  

app.use(express.json())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

app.use('/', require('./routes/login'))
app.use('/tarefa', require('./routes/api/tarefa'))

app.listen(PORT, () => console.log("Listenou"))